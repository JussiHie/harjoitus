﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testausta
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            Laskin laskin1 = new Laskin();
            Console.WriteLine("Summa: " + laskin1.Summa(5, 4));

            Console.WriteLine("Summa: " + laskin1.Summa(5, 5));

            Console.WriteLine("Summa: " + laskin1.Summa(5, 6));

            Console.WriteLine("Erotus: " + laskin1.Erotus(9, 1));
        }
    }
}
