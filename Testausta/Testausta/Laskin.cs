﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testausta
{
    class Laskin
    {
        public int Summa(int a, int b)
        {
            return a + b;
        }

        public int Erotus(int a, int b)
        {
            return a - b;
        }
    }
}
